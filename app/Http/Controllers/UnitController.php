<?php

namespace App\Http\Controllers;

use App\Unit;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = DB::table('units')
            ->select('units.id AS UNIT_ID', 'users.id AS USER_ID', 'users.name AS NAME', 'users.contact_number AS PHONE')
            ->where('units.user_id', '!=', null)
            ->join('users', 'units.user_id', '=', 'users.id')
            ->get();
        $userArray = array();
        foreach ($user as $usr) {
            $userArray[$usr->UNIT_ID] = $usr;
        }
        // dd($userArray);
        $id = 0;
        $data = DB::table('units')->select('id AS uid')->whereNotNull('user_id')->get()->toArray();
        $data2 = json_decode(json_encode($data), true);
        $ownedUnit = array_column($data2, 'uid');
        return view('admin.unit_index', compact('id', 'ownedUnit', 'userArray'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $i=0;

        $user=Auth::user();
        $units= User::with('units')->where('id',$user->id)->first();
        // dd($units);
        foreach($units->units as $item)
        {
            $unitArr[$item->id] = $item;
            $i++;
        }

        return view('owner.unit_index',compact('units', 'unitArr','i'));

    }

    /**
     * Show the form for editing the specified resource.
     *Assign unit kepada owner
     *
     * @param  \App\unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function edit($idUrl)
    { }

    public function assign($idUrl)
    {
        $userData = DB::table('units')
            ->select('units.id AS UNIT_ID', 'users.id AS USER_ID', 'users.name AS NAME', 'users.contact_number AS PHONE')
            ->where('units.user_id', '!=', null)
            ->join('users', 'units.user_id', '=', 'users.id')
            ->get();
        $userArray = array();
        foreach ($userData as $usr) {
            $userArray[$usr->UNIT_ID] = $usr;
        }
        $user = User::with('units')->where('id', $idUrl)->get();
        // dd($userData);
        // $user = $idUrl;
        $id = 0;
        $data = DB::table('units')->select('id AS uid')->whereNotNull('user_id')->get()->toArray();
        $data2 = json_decode(json_encode($data), true);
        $ownedUnit = array_column($data2, 'uid');
        return view('admin.assign_unit', compact('user', 'id', 'ownedUnit', 'userArray'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, unit $unit)
    {
        //
    }

    public function assignUnit(Request $request)
    {
        $user = User::find($request->userID)->get();



        $units = $request->input('unitID');
        foreach ($units as $unit) {
            DB::table('units')
                ->where('id', $unit)
                ->update(['user_id' => $request->userID]);
        }

        return redirect('/admin/user')->with('success', 'Unit/s have been assigned to ' . $user->name);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function destroy(unit $unit)
    {
        //
    }

    public function showEachUnit($id)
    {
        $unitArr = array();

        $user = Auth::user();
        $units = User::with('units')->where('id', $user->id)->first();
        // $eachUnit = $units->units->id == $id;
        foreach($units->units as $item)
        {
            $unitArr[$item->id] = $item;
        }
        // dd($unitArr[$id]);
        $selectedUnit=$unitArr[$id];

        if($selectedUnit->status == "rented")
        {
            $currentowner = DB::table('tenants')
                            ->join('units', 'tenants.id', '=',  'units.tenant_id')
                            ->where('units.id', $id)
                            ->first();
            // dd($currentowner);
        }
        else if($selectedUnit->status == "own stay")
        {
            $currentowner = DB::table('users')
                            ->join('units', 'users.id', '=',  'units.user_id')
                            ->where('units.id', $id)
                            ->first();
            // dd($currentowner);
        }
        else {
            $currentowner = "This unit status has not been updated yet!";
        }

        // dd($currentowner);
        return view('owner.unit_status', compact('selectedUnit', 'currentowner'));

        
    }

    public function updateStatus(Request $request, $id)
    {
        $unit = Unit::find($id);
        
        
        if($request->get('status') == "rented")
        {
            return redirect('/owner/tenant/create/'.$id);
        }else{
            $unit->status = $request->get('status');
            $unit->save();
            return redirect('/owner/unit')->with('success', 'You have successfully update unit '. $unit->block . '-' . $unit->level . '-' . $unit->number . ' status');
        }
    }

    public function reupdateStatus(Request $request, $id)
    {
        $unit = Unit::find($id);
        $unit->status = NULL;
        $unit->save();
        return redirect('/owner/unit')->with('success', 'You have successfully update unit '. $unit->block . '-' . $unit->level . '-' . $unit->number . ' status');
    }


}

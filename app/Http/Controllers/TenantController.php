<?php

namespace App\Http\Controllers;

use App\Tenant;
use App\User;
use App\Unit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class TenantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $unitArr = array();

        $user = Auth::user();
        $units = User::with('units')->where('id', $user->id)->first();
        // $eachUnit = $units->units->id == $id;
        foreach($units->units as $item)
        {
            $unitArr[$item->id] = $item;
        }
        // dd($unitArr[$id]);
        $selectedUnit=$unitArr[$id];
        return view('tenant.create', compact('selectedUnit'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'ic_passport' => ['required', 'string', 'max:255'],
            'citizenship' => ['required', 'string'],
            'contact_number' => ['required', 'numeric', 'min:10'],
            'duration' => ['required'],
        ]);

        $insertedData = Tenant::create([
            'name' => $validatedData['name'],
            'ic_passport' => $validatedData['ic_passport'],
            'citizenship' => $validatedData['citizenship'],
            'contact_number' => $validatedData['contact_number'],
            'duration' => $validatedData['duration'],
        ]);

        $unit = Unit::find($id);
        $unit->tenant_id = $insertedData->id;
        $unit->status = "rented";
        $unit->save();

        return redirect('/owner/unit')->with('success', 'New tenant has been added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tenant  $tenant
     * @return \Illuminate\Http\Response
     */
    public function show(tenant $tenant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tenant  $tenant
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tenant = DB::table('tenants')
                ->join('units','tenants.id', '=', 'units.tenant_id')
                ->where('tenants.id',$id)
                ->first();

                // dd($tenant);
        return view ('tenant.edit',compact('tenant'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tenant  $tenant
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id)
    {
        $tenant = Tenant::find($id);
        $tenantUnit = DB::table('tenants')
                ->join('units','tenants.id', '=', 'units.tenant_id')
                ->where('tenants.id',$id)
                ->first();

        $validatedData = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'ic_passport' => ['required', 'string', 'max:255'],
            'citizenship' => ['required', 'string'],
            'contact_number' => ['required', 'numeric', 'min:10'],
            'duration' => ['required'],
        ]);

        $tenant->name = $validatedData['name'];
        $tenant->ic_passport = $validatedData['ic_passport'];
        $tenant->citizenship = $validatedData['citizenship'];
        $tenant->contact_number = $validatedData['contact_number'];
        $tenant->duration = $validatedData['duration'];

        $tenant->save();

        return redirect('/owner/unit/' . $tenantUnit->id)->with('success', 'Tenant information has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tenant  $tenant
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $allunit = Unit::where('tenant_id', $id)->get();
        foreach($allunit as $unit)
        {
            $unit->status = null;
            $unit->save();
        }
        $user = Tenant::find($id);
        $user->delete($id);
        return redirect('/owner/unit')->with('success','Tenant has been deleted successfully');
    }
}

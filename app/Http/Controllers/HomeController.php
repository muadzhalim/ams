<?php

namespace App\Http\Controllers;

use App\User;
use App\Unit;
use Carbon\Traits\Units;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (auth()->user()->isAdmin == 1) {
            return redirect('/admin');
        } else {
            return redirect('/owner/unit');
        }
    }

    public function admin()
    {
        $userCount = User::where('isAdmin', 0)->get()->count();
        $unitCount = DB::table('units')->where('user_id', '!=', null)->get()->count();
        $allunit = Unit::get()->count();

        return view('admin.home', compact('userCount', 'unitCount', 'allunit'));
    }
}

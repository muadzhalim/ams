<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\User;
use App\Unit;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $i=1;
        $users = User::with('units')->where('isAdmin', 0)->get();
        //dd($users);
        return view('admin.user', compact('users', 'i'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'contact_number' => ['required', 'numeric', 'min:10', 'unique:users'],
        ]);

        User::create([
            'name' => $validatedData['name'],
            'email' => $validatedData['email'],
            'password' => Hash::make($validatedData['password']),
            'contact_number' => $validatedData['contact_number'],
        ]);

        return redirect('/admin/user')->with('success', 'New owner has been added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response

     */
    public function show()
    {
        if(Auth::check()){
            $id=auth()->user()->id;
            $user=User::find($id);
            //dd($user);
            return view('owner.profile' , compact('user'));
        }
        else{
            return redirect('/login');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = DB::table('users')->where('id', $id)->first();
        return view('admin.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $users = User::find($id);

            $validatedData = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'contact_number' => ['required', 'numeric', 'min:10'],
        ]);

        $users->name = $validatedData['name'];
        $users->email = $validatedData['email'];
        $users->contact_number = $validatedData['contact_number'];

        $users->save();

        if(auth()->user()->isAdmin==1){
            return redirect('/admin/user')->with('success', 'User has been updated');
        }
        else{
            return redirect('/owner/profile')->with('success', 'Profile has been updated');
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        

        $allunit = Unit::where('user_id', '$id')->get();
        foreach($allunit as $unit)
        {
            $unit->user_id = NULL;
            $unit->save();
        }
        $user->delete($id);
        return redirect('/admin/user')->with('success','User has been deleted successfully');
    }

    public function password()
    {
        return view('owner.changepassword');
    }

    public function updatepassword(Request $request)
    {
        if (!(Hash::check($request->get('cpw'), Auth::user()->password))) {
                    // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }
            if(strcmp($request->get('cpw'), $request->get('npw')) == 0){
                    //Current password and new password are same 1 true 0 false
                 return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose different password.");
            }
            $validatedData = $request->validate([
                'cpw' => 'required',
                'npw' => 'required|string|min:8',
                'confirmpw' => 'required|string|min:8|same:npw',
            ]);
            //Change Password Cari User yang nak tukar
            $user = Auth::user();
            $user->password = bcrypt($request->get('npw'));
            $user->save();

            return redirect()->back()->with("success","Password changed successfully !");
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tenant()
    {
        return $this->hasOne(Tenant::class);
    }

    public static function boot() {
        parent::boot();

        static::deleting(function($unit) { // before delete() method call this
             $unit->tenant()->delete();
             // do the rest of the cleanup...
        });
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Tenant extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'name', 'ic_passport', 'citizenship','contact_number','duration'
    ];

    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }
}

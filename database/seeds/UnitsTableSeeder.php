<?php

use Illuminate\Database\Seeder;
use App\Unit;

class UnitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($level=1; $level<6; $level++) {
            for($no=1;$no<11;$no++){
                Unit::create([
                    'number' => $no,
                    'level' => $level,
                    'block' => "A",
                ]);
            }
        }
        for($level=1; $level<6; $level++) {
            for($no=1;$no<11;$no++){
                Unit::create([
                    'number' => $no,
                    'level' => $level,
                    'block' => "B",
                ]);
            }
        }
        for($level=1; $level<6; $level++) {
            for($no=1;$no<11;$no++){
                Unit::create([
                    'number' => $no,
                    'level' => $level,
                    'block' => "C",
                ]);
            }
        }
        for($level=1; $level<6; $level++) {
            for($no=1;$no<11;$no++){
                Unit::create([
                    'number' => $no,
                    'level' => $level,
                    'block' => "D",
                ]);
            }
        }
        for($level=1; $level<6; $level++) {
            for($no=1;$no<11;$no++){
                Unit::create([
                    'number' => $no,
                    'level' => $level,
                    'block' => "E",
                ]);
            }
        }
    }
}

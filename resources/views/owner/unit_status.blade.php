@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Manage Unit Status <strong>{{$selectedUnit->block}}-{{$selectedUnit->level}}-{{$selectedUnit->number}}</strong> </div>

                <div class="card-body">
                        @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                        @endif
                    @if ($selectedUnit->status == null)
                    <h3 class="my-3 text-center">Please choose status for : <strong>{{$selectedUnit->block}}-{{$selectedUnit->level}}-{{$selectedUnit->number}}</strong></h3>
                    <form action="{{route('owner.updateStatus', $selectedUnit->id)}}" method="POST">
                        @csrf
                        @method('PATCH')
                        <div class="statusSelection text-center">
                            <label>
                                <input type="radio" name="status" value="rented"> 
                                <div  class="btnc btn-custom"><span>Rented Out</span></div>
                            </label>
                            <label>
                                <input type="radio" name="status" value="own stay"> 
                                <div  class="btnc btn-custom"><span>Own Stay</span></div>
                            </label>
                            <div class="my-3"></div>
                            <input class="btn btn-primary" type="submit" value="Submit">
                        </div>
                    </form>
                    @else
                    <h3 class="my-3 text-center">Status for : <strong>{{$selectedUnit->block}}-{{$selectedUnit->level}}-{{$selectedUnit->number}}</strong></h3>
                    <div class="col-sm-12 my-3">
                        <div class="card">
                        <div class="card-body">
                            <h2 class="card-title"><strong>{{$selectedUnit->block}}-{{$selectedUnit->level}}-{{$selectedUnit->number}}</strong>   <span class="badge badge-primary">{{ucwords($selectedUnit->status)}}</span></h2>
                            
                            @if ($selectedUnit->status == "rented")
                                <h3>Tenant Information</h3>
                                <p>Name : {{$currentowner->name}}</p>
                                <p>IC/Passport : {{$currentowner->ic_passport}}</p>
                                <p>Citizenship : {{$currentowner->citizenship}}</p>
                                <p>Contact Number : {{$currentowner->contact_number}}</p>
                                <p>Duration : {{$currentowner->duration}}</p>
                                
                                <a class="btn btn-primary" href="{{route('tenant.edit',$currentowner->tenant_id)}}" role="button">Edit Tenant</a>

                                <form method="POST" action="{{ route('tenant.delete', $currentowner->tenant_id) }}">
                                        @csrf
                                        @method('DELETE')
                                        <div>
                                            <button type="submit" onclick="return confirm('Really delete this tenant?')" class="mt-1 btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete Tenant"><ion-icon name="trash"></ion-icon></button>
                                        </div>
                                </form>

                            @elseif ($selectedUnit->status == "own stay")
                                <h3>Owner Information</h3>
                                <p>Name : {{$currentowner->name}}</p>
                                <p>Email : {{$currentowner->email}}</p>
                                <p>Contact Number : {{$currentowner->contact_number}}</p>
                                    
                                    <form method="POST" action="{{ route('owner.reupdateStatus', $currentowner->id) }}">
                                        @csrf
                                        @method('PATCH')
                                        <input type="hidden" name="status" value="NULL">
                                        <div>
                                            <button type="submit" onclick="return confirm('Really change this unit status?')" class="mt-1 btn btn-info">Change Unit Status</button>
                                        </div>
                                    </form>

                            @else 
                                <h3>This unit's status is not updated yet</h3>
                            @endif
                        </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Unit List- {{$units->name}} </div>
                <div class="card-body">
                        @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                        @endif
                        @if (session('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error') }}
                        </div>
                        @endif
                    <h5>You have {{$i}} units in Anggerik Apartment.</h5>
                    <div class="row ">
                        @foreach ($units->units as $item)
                        <div class="col-sm-6 my-3">
                            <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">{{$item->block}}-{{$item->level}}-{{$item->number}}</h5>
                                <h3>
                                    @if($unitArr[$item->id]->status == null)
                                        <span data-toggle="tooltip" data-placement="right" title="Please manage this unit" class="badge badge-danger"><ion-icon name="alert"></ion-icon></span>
                                    @endif
                                        <span class="badge badge-warning">{{ucwords($unitArr[$item->id]->status)}}</span>
                                </h3>
                                <a href="{{route('owner.eachunit',$item->id)}}" class="btn btn-primary">Manage Unit</a>
                            </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
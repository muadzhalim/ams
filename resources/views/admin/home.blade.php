@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
                @endif
                <div class="card-header">Admin Dashboard</div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Current Owners Count</h5>
                                <p class="card-text">{{$userCount}} owners.</p>
                                <a href="{{route('admin.user')}}" class="btn btn-primary">View Owners List</a>
                            </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">Current Units Owned Counts</h5>
                                <p class="card-text">{{$unitCount}} units out of {{$allunit}}.</p>
                                <a href="{{route('admin.unit')}}" class="btn btn-primary">View Units List</a>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
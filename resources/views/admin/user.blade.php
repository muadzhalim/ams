@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
              <div class="card-header">Users List</div>

                <div class="card-body">
                    @if (session('success'))
                    <div class="alert alert-success" role="alert">
                        {{ session('success') }}
                    </div>
                    @endif
                    @if (session('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error') }}
                        </div>
                    @endif
                        <a class="btn btn-primary float-right mb-2" href="{{route('admin.create')}}" role="button">Register New Owner</a>
                        <table class="table table-striped">
                                <thead>
                                  <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Contact Number</th>
                                    <th scope="col">Owned Units</th>
                                    <th scope="col">Action</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  @foreach ($users as $usr)
                                  <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$usr->name}}</td>
                                    <td>{{$usr->email}}</td>
                                    <td>{{$usr->contact_number}}</td>
                                    <td>
                                        @foreach($usr->units as $tblUnits)
                                        <p><span class="badge badge-primary">{{$tblUnits->block}}-{{$tblUnits->level}}-{{$tblUnits->number}}</span></p>
                                        @endforeach

                                    </td>
                                
                                    <td>
                                        <a data-toggle="tooltip" data-placement="top" title="Edit User" href="{{route('admin.edit', $usr->id)}}" class="btn btn-warning"><ion-icon name="create" ></ion-icon></a>
                                         <a data-toggle="tooltip" data-placement="top" title="Assign Unit" href="{{route('unit.assign', $usr->id)}}" class="btn btn-success"><ion-icon name="home"  ></ion-icon></a>
                                        <form method="POST" action="{{ route('admin.delete', $usr->id) }}">
                                                @csrf
                                                @method('DELETE')
                                                <div>
                                                    <button type="submit" onclick="return confirm('Really delete this user?')" class="mt-1 btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete User"><ion-icon name="trash"></ion-icon></button>
                                                </div>
                                        </form>
                                    </td>
                                  </tr>
                                  @endforeach
                                </tbody>
                              </table>               
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

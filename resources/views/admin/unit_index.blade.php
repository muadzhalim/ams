@extends('layouts.app')

@section('content')

<div class="container">
    @if (session('error'))
    <div class="alert alert-danger" role="alert" data-dismiss="alert">
        {{ session('error') }}
    </div>
    @endif
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Unit List</div>

                <div class="card-body">
                    <div class="d-flex justify-content-center mb-4">
                        <button id="blockAbtn" class="btn btn-primary mr-3">Block A</button>
                        <button id="blockBbtn" class="btn btn-primary mr-3">Block B</button>
                        <button id="blockCbtn" class="btn btn-primary mr-3">Block C</button>
                        <button id="blockDbtn" class="btn btn-primary mr-3">Block D</button>
                        <button id="blockEbtn" class="btn btn-primary mr-3">Block E</button>
                    </div>

                        {{-- Block A --}}

                        <div class="container" id="blockA">
                            <h3>Block A</h3>
                            @for($i=0;$i<5;$i++)
                            <div id="floor{{$i+1}}">
                                <h3><span class="badge badge-info">Floor {{$i+1}}</span></h3>
                                <div class="row justify-content-center">
                                    @for($x=0;$x<10;$x++)
                                    <div class="col-1 m-1 square text-center">
                                        <input id="unit{{$id+1}}" name="unitID[]" type="checkbox" value="{{$id+1}}"/> 
                                        <label for="unit{{$id+1}}"
                                            @if ($id+1 == isset($userArray[$id+1]->USER_ID))
                                            data-toggle="tooltip" data-placement="top" data-html="true" title="{{$userArray[$id+1]->NAME}} <br> {{$userArray[$id+1]->PHONE}}" 
                                            @endif>{{$x+1}}
                                        </label>
                                        @php
                                            $id++
                                        @endphp
                                    </div>
                                    @endfor
                                </div>
                            </div>
                            @endfor
                        </div>

                        {{-- Block B --}}

                        <div class="container" id="blockB">
                            <h3>Block B</h3>
                            @for($i=0;$i<5;$i++)
                            <div id="floor{{$i+1}}">
                                <h3><span class="badge badge-info">Floor {{$i+1}}</span></h3>
                                <div class="row justify-content-center">
                                    @for($x=0;$x<10;$x++)
                                    <div class="col-1 m-1 square text-center">
                                            <input class="chk-btn" id="unit{{$id+1}}" type="checkbox" name="unitID[]" value="{{$id+1}}"/> 
                                            <label for="unit{{$id+1}}"
                                                @if ($id+1 == isset($userArray[$id+1]->USER_ID))
                                                data-toggle="tooltip" data-placement="top" data-html="true" title="{{$userArray[$id+1]->NAME}} <br> {{$userArray[$id+1]->PHONE}}" 
                                                @endif>{{$x+1}}
                                            </label>
                                        @php
                                            $id++
                                        @endphp
                                    </div>
                                    @endfor
                                </div>
                            </div>
                            @endfor
                        </div>

                        {{-- Block C --}}

                        <div class="container" id="blockC">
                            <h3>Block C</h3>
                            @for($i=0;$i<5;$i++)
                            <div id="floor{{$i+1}}">
                                <h3><span class="badge badge-info">Floor {{$i+1}}</span></h3>
                                <div class="row justify-content-center">
                                    @for($x=0;$x<10;$x++)
                                    <div class="col-1 m-1 square text-center">
                                        <input class="chk-btn" id="unit{{$id+1}}" type="checkbox" name="unitID[]" value="{{$id+1}}"/> 
                                        <label for="unit{{$id+1}}"
                                            @if ($id+1 == isset($userArray[$id+1]->USER_ID))
                                            data-toggle="tooltip" data-placement="top" data-html="true" title="{{$userArray[$id+1]->NAME}} <br> {{$userArray[$id+1]->PHONE}}" 
                                            @endif>{{$x+1}}
                                        </label>
                                        @php
                                            $id++
                                        @endphp
                                    </div>
                                    @endfor
                                </div>
                            </div>
                            @endfor
                        </div>

                        {{-- Block D --}}

                        <div class="container" id="blockD">
                            <h3>Block D</h3>
                            @for($i=0;$i<5;$i++)
                            <div id="floor{{$i+1}}">
                                <h3><span class="badge badge-info">Floor {{$i+1}}</span></h3>
                                <div class="row justify-content-center">
                                    @for($x=0;$x<10;$x++)
                                    <div class="col-1 m-1 square text-center">
                                        <input class="chk-btn" id="unit{{$id+1}}" type="checkbox" name="unitID[]" value="{{$id+1}}"/> 
                                        <label for="unit{{$id+1}}"
                                            @if ($id+1 == isset($userArray[$id+1]->USER_ID))
                                            data-toggle="tooltip" data-placement="top" data-html="true" title="{{$userArray[$id+1]->NAME}} <br> {{$userArray[$id+1]->PHONE}}" 
                                            @endif>{{$x+1}}
                                        </label>
                                        @php
                                            $id++
                                        @endphp
                                    </div>
                                    @endfor
                                </div>
                            </div>
                            @endfor
                        </div>

                        {{-- Block E --}}

                        <div class="container" id="blockE">
                            <h3>Block E</h3>
                            @for($i=0;$i<5;$i++)
                            <div id="floor{{$i+1}}">
                                <h3><span class="badge badge-info">Floor {{$i+1}}</span></h3>
                                <div class="row justify-content-center">
                                    @for($x=0;$x<10;$x++)
                                    <div class="col-1 m-1 square text-center">
                                        <input class="chk-btn" id="unit{{$id+1}}" type="checkbox" name="unitID[]" value="{{$id+1}}"/> 
                                        <label for="unit{{$id+1}}"
                                            @if ($id+1 == isset($userArray[$id+1]->USER_ID))
                                            data-toggle="tooltip" data-placement="top" data-html="true" title="{{$userArray[$id+1]->NAME}} <br> {{$userArray[$id+1]->PHONE}}" 
                                            @endif>{{$x+1}}
                                        </label>
                                        @php
                                            $id++
                                        @endphp
                                    </div>
                                    @endfor
                                </div>
                            </div>
                            @endfor
                        </div>


                </div>
            </div>
        </div>
    </div>
</div>
<script type="application/javascript"> 
$(document).ready(function() {

    let owned = {{ json_encode($ownedUnit)}};
    owned.forEach(own => {
        document.getElementById("unit" + own).disabled = true;
    });


    $("#blockB").hide();
    $("#blockC").hide();
    $("#blockD").hide();
    $("#blockE").hide();

    $("#blockAbtn").on("click", function(){
        $("#blockA").show();
        $("#blockB").hide();
        $("#blockC").hide();
        $("#blockD").hide();
        $("#blockE").hide();
    });

    $("#blockBbtn").on("click", function(){
        $("#blockA").hide();
        $("#blockB").show();
        $("#blockC").hide();
        $("#blockD").hide();
        $("#blockE").hide();
    });

    $("#blockCbtn").on("click", function(){
        $("#blockA").hide();
        $("#blockB").hide();
        $("#blockC").show();
        $("#blockD").hide();
        $("#blockE").hide();
    });

    $("#blockDbtn").on("click", function(){
        $("#blockA").hide();
        $("#blockB").hide();
        $("#blockC").hide();
        $("#blockD").show();
        $("#blockE").hide();
    });

    $("#blockEbtn").on("click", function(){
        $("#blockA").hide();
        $("#blockB").hide();
        $("#blockC").hide();
        $("#blockD").hide();
        $("#blockE").show();
    });

});
</script>
@endsection


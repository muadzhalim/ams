@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                    
                <div class="card-header">Edit Tenant</div>

                <div class="card-body">
                        @if (session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                        @endif
                        <form action="{{route('tenant.update', $tenant->tenant_id)}}" method="POST">
                            @csrf
                            @method('PATCH')
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{$tenant->name}}" required autocomplete="name" autofocus>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                           <div class="form-group row">
                                <label for="ic_passport" class="col-md-4 col-form-label text-md-right">IC/Passport</label>
    
                                <div class="col-md-6">
                                    <input id="ic_passport" type="text" class="form-control @error('ic_passport') is-invalid @enderror" name="ic_passport" value="{{$tenant->ic_passport }}" required autocomplete="ic_passport" autofocus>
    
                                    @error('ic_passport')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
    
                            <div class="form-group row">
                                <label for="contact-number" class="col-md-4 col-form-label text-md-right">Contact Number</label>
    
                                <div class="col-md-6">
                                    <input id="contact-number" type="text" class="form-control @error('contact_number') is-invalid @enderror" name="contact_number" value="{{ $tenant->contact_number }}" required autocomplete="contact_number">
                                
                                    @error('contact_number')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
    
                            <div class="form-group row">
                                <label for="citizenship" class="col-md-4 col-form-label text-md-right">Citizenship</label>
    
                                <div class="col-md-6">
                                    <input id="citizenship" type="text" class="form-control @error('citizenship') is-invalid @enderror" name="citizenship" required autocomplete="citizenship" value="{{ $tenant->citizenship }}">
                                    @error('citizenship')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
    
                            <div class="form-group row">
                                <label for="duration" class="col-md-4 col-form-label text-md-right">Duration</label>
    
                                <div class="col-md-6">
                                    <select class="form-control @error('duration') is-invalid @enderror" name="duration" value="{{ $tenant->duration }}" id="duration">
                                        <option value="1 month">One month</option>
                                        <option value="3 month">Three months</option>
                                        <option value="6 month">Six months</option>
                                        <option value="1 year">One year</option>
                                        </select>
                                    @error('duration')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
    
                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            Save Tenant
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

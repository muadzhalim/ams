<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//Admin
Route::get('/admin', 'HomeController@admin')->name('admin.home')->middleware('admin');
Route::get('/admin/user', 'UserController@index')->name('admin.user')->middleware('admin');
Route::get('/admin/user/edit/{id}', 'UserController@edit')->name('admin.edit')->middleware('admin');
Route::patch('/admin/user/edit/{id}', 'UserController@update')->name('admin.update');
Route::delete('/admin/user/delete/{id}', 'UserController@destroy')->name('admin.delete')->middleware('admin');
Route::get('/admin/user/create/', 'UserController@create')->name('admin.create')->middleware('admin');
Route::post('/admin/user/create/', 'UserController@store')->name('admin.store')->middleware('admin');
Route::get('admin/unit','UnitController@index')->name('admin.unit')->middleware('admin');
Route::get('admin/unit/assign/{id}','UnitController@assign')->name('unit.assign')->middleware('admin');
Route::patch('admin/unit/assign/{id}','UnitController@assignUnit')->name('unit.assignUnit')->middleware('admin');

//Owner
Route::get('/owner/profile', 'UserController@show')->name('owner.profile')->middleware('owner');
Route::get('/owner/profile/edit/{id}','UserController@edit')->name('owner.edit')->middleware('owner');
Route::patch('/owner/profile/edit/{id}', 'UserController@update')->name('owner.update')->middleware('owner');
Route::get('/owner/change-password','UserController@password')->name('owner.password')->middleware('owner');
Route::patch('/owner/change-password','UserController@updatepassword')->name('owner.updatepassword')->middleware('owner');
Route::get('/owner/unit','UnitController@show')->name('owner.unit')->middleware('owner');
Route::get('/owner/unit/{id}', 'UnitController@showEachUnit')->name('owner.eachunit')->middleware('owner');
Route::patch('/owner/unit/{id}','UnitController@updateStatus')->name('owner.updateStatus')->middleware('owner');
Route::patch('/owner/unit/reupdate/{id}','UnitController@reupdateStatus')->name('owner.reupdateStatus')->middleware('owner');

//Tenant
Route::get('/owner/tenant/create/{id}', 'TenantController@create')->name('tenant.create')->middleware('owner');
Route::post('/owner/tenant/create/{id}', 'TenantController@store')->name('tenant.store')->middleware('owner');
Route::delete('/owner/tenant/delete/{id}', 'TenantController@destroy')->name('tenant.delete')->middleware('owner');
Route::get('/owner/tenant/edit/{id}','TenantController@edit')->name('tenant.edit')->middleware('owner');
Route::patch('/owner/tenant/edit/{id}', 'TenantController@update')->name('tenant.update')->middleware('owner');
